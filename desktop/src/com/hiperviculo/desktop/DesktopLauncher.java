package com.hiperviculo.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.hiperviculo.Controles.MainClass;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Hipervinculo";
		config.height = 960;
		config.width = 960;
		config.vSyncEnabled = true;
		config.resizable = false;


		new LwjglApplication(new MainClass(), config);
	}
}
