package com.hiperviculo.Objetos;

import com.hiperviculo.Pantallas.GameScreen;

public class CasillasMapa {



    private int ancho, alto;
    private Casillas[][] casillas;
    private Player player;
    private GameScreen gameScreen;

    public CasillasMapa(int ancho, int alto){

        this.ancho = ancho;
        this.alto = alto;
        casillas =  new Casillas[ancho][alto];
        for(int x=0; x< ancho; x++){
            for(int y=0; y < alto; y++){
                //Aqui se pondria si queremos poner varias texturas
                casillas[x][y] = new Casillas(TERRENO.SUELOCUEVA);

            }
        }
    }

    public Casillas getCasillas(int x, int y){
        return casillas[x][y];

    }



    public int getAncho() {
        return ancho;
    }

    public int getAlto() {
        return alto;
    }
}
