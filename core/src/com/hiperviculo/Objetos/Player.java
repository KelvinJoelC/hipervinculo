package com.hiperviculo.Objetos;

import com.hiperviculo.Pantallas.GameScreen;

public class Player {

    private CasillasMapa mapa;
    private Casillas casilla;
    private GameScreen gameScreen;
    private Casillas terreno;

    //Que posicion se encuentra el personaje
    private int x;
    private int y;
    private int coordObjetoX;
    private int coordObjetoY;
    private boolean coord;

    public Player(CasillasMapa mapa,int x, int y) {
        this.mapa = mapa;
        this.x = x;
        this.y = y;
        mapa.getCasillas(x,y).setPlayer(this);
    }


    public boolean move(int dx, int dy){

        if(x+dx >= mapa.getAncho()-1 || x+dx <= 0 ||
                (y+dy >= mapa.getAlto()-2 && x+dx<=10)||
                (y+dy >= mapa.getAlto()-2 && x+dx>=12)
                || y+dy <=1){
            return false;
        }

        if(x+dx == coordObjetoX && y+dx == coordObjetoY){
            return false;
        }/*
        if(mapa.getCasillas(x+dx, y+dy)  ){
            return false;
        }*/

        x += dx;
        y += dy;

        mapa.getCasillas(x,y).setPlayer(this);
        return true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
