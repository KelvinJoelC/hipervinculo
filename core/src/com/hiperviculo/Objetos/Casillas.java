package com.hiperviculo.Objetos;

public class Casillas {

    private TERRENO terreno;

    private Player player;


    public Casillas(TERRENO terreno) {
        this.terreno = terreno;
    }

    public TERRENO getTerreno() {
        return terreno;
    }

    public void setTerreno(TERRENO terreno) {
        this.terreno = terreno;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

}
