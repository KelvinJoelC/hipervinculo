package com.hiperviculo.Objetos;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.hiperviculo.Controles.MainClass;

public class Objetos extends MainClass {

    private Texture antorcha;
    private SpriteBatch batch;
    private Sprite sprite;
    private TextureRegion[] regiones;
    private Animation animation;
    private float tiempo;
    private TextureRegion frameActual;

    public Objetos (){
        antorcha = new Texture("torch2.png");
        batch = new SpriteBatch();

        sprite = new Sprite (antorcha = new Texture(Gdx.files.internal("torch2.png")),80,20);
        TextureRegion[][] tmp = TextureRegion.split(antorcha,antorcha.getWidth()/12, antorcha.getHeight());       //Metemos el sprite en el array bidimensional

        regiones = new TextureRegion[12];                       //Metemos cada sprite en un array
        for (int i=0; i<12; i++) regiones[i] = tmp[0][i];       //Hacemos el array unidimensional(temas de eficiencia)
        animation = new Animation(1/10f,regiones);
        tiempo = 0f;
    }

    public void render(final SpriteBatch batch){
        tiempo += Gdx.graphics.getDeltaTime();
        frameActual = (TextureRegion) animation.getKeyFrame(tiempo,true);
        batch.draw(frameActual,500,300);
    }

}
