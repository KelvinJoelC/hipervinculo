package com.hiperviculo.Controles;

public class Camara {

    private float camaraX= 0f;



    private float camaraY = 0f;

    public void update(float newCamX, float newCamY){
        this.camaraX = newCamX;
        this.camaraY = newCamY;

    }
    public float getCamaraX() {
        return camaraX;
    }

    public void setCamaraX(float camaraX) {
        this.camaraX = camaraX;
    }

    public float getCamaraY() {
        return camaraY;
    }

    public void setCamaraY(float camaraY) {
        this.camaraY = camaraY;
    }
}
