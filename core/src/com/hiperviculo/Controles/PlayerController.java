package com.hiperviculo.Controles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hiperviculo.Objetos.Player;
import com.hiperviculo.Pantallas.GameScreen;

public class PlayerController extends InputAdapter {

    private Player player;

    public PlayerController(Player jugador){
        this.player = jugador;
    }

    @Override
    public boolean keyDown(int boton) {
        if (boton == Keys.UP || boton == Input.Keys.W){
            player.move(0,1);
        }
        if (boton == Keys.DOWN || boton == Input.Keys.S){
            player.move(0,-1);
        }
        if (boton == Keys.RIGHT || boton == Input.Keys.D){
            player.move(1,0);
        }
        if (boton == Keys.LEFT || boton == Input.Keys.A){
            player.move(-1,0);
        }


        return false;
    }
}
