package com.hiperviculo.Controles;

public class Settings {


    public static int TILE_SIZE = 20;   // Tamaño de cada casilla (Zoom)
    public static float SCALE = 2f;
    public static float SCALED_TILE_SIZE = TILE_SIZE * SCALE;  // Tamaño ya escalado

}
