package com.hiperviculo.Controles;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.hiperviculo.Objetos.Objetos;
import com.hiperviculo.Objetos.Player;
import com.hiperviculo.Pantallas.*;
import jdk.nashorn.internal.objects.annotations.Function;

public class MainClass extends Game {

	private SpriteBatch batch;
	private int width, height;                            //variables que diran la medida de la pantalla
	Texture texture;

	public static boolean trans = true;
	public static boolean trans2 = true;
	public static boolean trans3 = true;
	public static boolean trans4 = true;
	public static boolean trans5 = true;
	public static boolean trans6 = true;

	public static boolean personaje = true;
	public static boolean muerto = true;
	public static boolean ganado = true;
	public static boolean reiniciar = true;

	public Screen intro, ganar, muerte, screen, screen1, screen2, screen3, screen4;
	public static int movRightLeft = 0;
	public static int movTopBot = 0;


	@Override
	public void create() {
		batch = new SpriteBatch();
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		texture = new Texture("menu.jpg");


		ganar = new GameEnd(this);
		muerte = new GameMuerte(this);
		intro = new GameIntro(this);
		screen = new GameScreen(this);
		screen2 = new MiddleScreen(this);
		screen3 = new FinalScreen(this);

		this.setScreen(intro);
	}

	@Override
	public void render() {

			Gdx.gl.glClearColor(0, 0.6f, 0.3f, 1); // Color del fondo de pantalla
			//Gdx.gl.glActiveTexture();
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiar la targeta gráfica
			super.render();

			batch.begin();
			transiccionInicio();
			transiccionGameScreen();
			transiccionMuerte();
			transiccionGanar();
			transiccionInicio();
			batch.end();

	}

	@Override
	public void dispose() {
		batch.dispose();

		screen1.dispose();
		screen2.dispose();
		intro.dispose();

	}

	private void transiccionGameScreen() {
		if (trans == false) {
			this.setScreen(screen2);
			trans = true;
		}
		if (trans4 == false) {
			this.setScreen(screen3);
			trans4 = true;
		}
	}

	public void transiccionInicio() {
		if (trans3 == false) {
			this.setScreen(screen);
			trans3 = true;
		}
		if (trans2 == false) {
			this.setScreen(screen);
			trans2 = true;
		}
	}


	private void transiccionMuerte() {
		if (muerto == false) {
			this.setScreen(muerte);
			trans5 = true;
		}
	}


	private void transiccionGanar() {
		if (ganado == false) {
			this.setScreen(ganar);
			trans6 = true;
		}
	}
}