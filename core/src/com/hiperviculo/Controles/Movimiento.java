package com.hiperviculo.Controles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class Movimiento extends MainClass {          //Creación de un actor

    private Sprite sprite;
    public int x,y;
    public int origin_x = 380;          //x inicial
    public int origin_y = 300;          //y inicial

    private Animation animation;
    private float tiempo;
    private TextureRegion[] regionsMovimiento;
    private Texture imagen;
    private TextureRegion frameActual;

    public Movimiento(int x, int y){
        this.x = x + origin_x;
        this.y = y + origin_y;
        sprite = new Sprite (imagen = new Texture(Gdx.files.internal("Animaciones/PersonajeSimple.png")),80,130);
        TextureRegion [][] tmp = TextureRegion.split(imagen,imagen.getWidth()/8, imagen.getHeight());       //Metemos el sprite en el array bidimensional

        regionsMovimiento = new TextureRegion[4];                       //Metemos cada sprite en un array
        for (int i=0; i<4; i++)
            regionsMovimiento[i] = tmp[0][i];       //Hacemos el array unidimensional(temas de eficiencia)
        animation = new Animation(1,regionsMovimiento);
        tiempo = 0f;
    }

    public void render(final SpriteBatch batch){
        //tiempo += Gdx.graphics.getDeltaTime();
        frameActual = (TextureRegion) animation.getKeyFrame(tiempo,true);
            if (Gdx.input.isKeyPressed(Input.Keys.D)){
                batch.draw(regionsMovimiento[0],x,y);
            }
            else if(Gdx.input.isKeyPressed(Input.Keys.A)){
                batch.draw(regionsMovimiento[2],x,y);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.W)){
                batch.draw(regionsMovimiento[2],x,y);
            }
            else if (Gdx.input.isKeyPressed(Input.Keys.S)){
                batch.draw(regionsMovimiento[0],x,y);
            }
            else
                batch.draw(regionsMovimiento[1],x,y);

    }


}

