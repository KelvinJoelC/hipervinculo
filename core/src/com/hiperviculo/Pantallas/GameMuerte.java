
package com.hiperviculo.Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.hiperviculo.Controles.MainClass;

public class GameMuerte extends AbstractScreen{

    private Stage stage;
    private Skin skin;

    public GameMuerte(MainClass main) { super(main); }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage = new Stage();

        //Creacion de la tabla que contendra los objetos, como el boton de play
        Table table =  new Table();
        table.setPosition(0, 0);
        table.setFillParent(true);
        table.setHeight(500);
        table.background(new NinePatchDrawable(getNinePatch("menu.jpg")));
        stage.addActor(table);

        //Etiqueta que tiene el titulo del juego
        Label label = new Label("HAS MUERTO", getSkin());
        label.setFontScale(3,3);
        label.setPosition(label.getOriginX() +350, label.getOriginY() +550);
        table.addActor(label);


        //Boton de salida
        TextButton Quit = new TextButton("Salir", getSkin());
        Quit.setPosition(label.getOriginX() +435, label.getOriginY() +330);
        Quit.setWidth(100);
        Quit.setHeight(50);
        Quit.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.exit(0);
                return false;
            }
        });
        table.addActor(Quit);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        Gdx.input.setInputProcessor(stage);
    }

    //Funcion que dara aspecto a nuestros elementos de la tabla
    protected Skin getSkin() {
        if (skin == null) {
            skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        }
        return skin;
    }

    private NinePatch getNinePatch(String fname) {
        final Texture imagen = new Texture(Gdx.files.internal(fname));
        return new NinePatch( new TextureRegion(imagen, 1, 1 , imagen.getWidth() - 0, imagen.getHeight() -0), 10, 10, 10, 10);
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }
}
