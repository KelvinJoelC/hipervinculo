package com.hiperviculo.Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hiperviculo.Controles.*;
import com.hiperviculo.Objetos.CasillasMapa;
import com.hiperviculo.Objetos.Player;
import sun.applet.Main;

public class GameScreen extends AbstractScreen {

    private PlayerController control;


    private Player player;
    private CasillasMapa mapa;
    private Texture nuevoPersonaje;
    private  Texture columna;
    private Texture Suelo;
    public SpriteBatch batch;
    private Texture enemigo2;
    private Texture trampa;
    private Texture enemigo3;
    private Texture laba;

    private int random1 = (int) (Math.random()*20);
    private int random2 = (int) (Math.random()*20);

    private int random3 = (int) (Math.random()*20);
    private int random4 = (int) (Math.random()*20);


    int AnchoMapa = 24, AltoMapa= 24 ;

    private Texture puerta;

    private int posXTrampa1 = 4, posYTrampa1 = 3;
    private int posXTrampa2 = 5, posYTrampa2 = 5;

    private boolean varenemigo = false;

    private int personajex =2, personajey =2;



    public GameScreen(MainClass main) {
        super(main);

        Suelo = new Texture("SueloCastillo.PNG");
        columna = new Texture("ColumnaCastillo.PNG");
        batch = new SpriteBatch();
        mapa = new CasillasMapa(AnchoMapa, AltoMapa);
        // Creo el mapa de 24x24
        player = new Player(mapa, AnchoMapa/2, AnchoMapa/2);
        // (El tamaño del las casillas por donde se moverá, x inicio, y inicio)
        control = new PlayerController(player);

        puerta = new Texture("puerta.png");
        enemigo2 = new Texture("enemigo2.png");
        trampa = new Texture("trampaResolucion1000.png");
        enemigo3 = new Texture("enemigo3.png");
        laba = new Texture("laba.png");


    }


    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(control);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Segun el boton de jugar seleccionado, cargara un personaje u otro
        if (MainClass.personaje == true ){
            nuevoPersonaje = new Texture("PersonajePrincipal.png");
        }
        if (MainClass.personaje == false){
            nuevoPersonaje = new Texture("Elbueno_Principal.png");
        }


        batch.begin();



        for(int x=0; x < mapa.getAncho(); x++){
            for(int y =0; y < mapa.getAlto(); y++){
                Texture render ;
                render = Suelo;
                batch.draw(render, x*Settings.SCALED_TILE_SIZE, y*Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE);
            }//(Textura que queremos cargar, X, Y, Ancho de como se va a ver la textura, Alto ... )

        }

        //Trampas
        batch.draw(trampa,Settings.SCALED_TILE_SIZE * random3, Settings.SCALED_TILE_SIZE *3,40,40);
        batch.draw(trampa,Settings.SCALED_TILE_SIZE * 2, Settings.SCALED_TILE_SIZE * random3,40,40);
        batch.draw(trampa,Settings.SCALED_TILE_SIZE * random2, Settings.SCALED_TILE_SIZE *8,40,40);
        batch.draw(trampa,Settings.SCALED_TILE_SIZE * 5, Settings.SCALED_TILE_SIZE * random1,40,40);

        //Enemigos
        batch.draw(enemigo3,Settings.SCALED_TILE_SIZE * 8, Settings.SCALED_TILE_SIZE * 15,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo3,Settings.SCALED_TILE_SIZE * random3, Settings.SCALED_TILE_SIZE * random3,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo3,Settings.SCALED_TILE_SIZE * random2, Settings.SCALED_TILE_SIZE * random1,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo2,Settings.SCALED_TILE_SIZE * random3, Settings.SCALED_TILE_SIZE * random4,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo2,Settings.SCALED_TILE_SIZE * random1, Settings.SCALED_TILE_SIZE * random2,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);


        //Introducimos el personaje, esto solo lo dibuja y representa
        for(int x=0; x < AnchoMapa; x++){
            for(int y =0; y < AltoMapa; y++) {
                if ((x== 0 || x==AltoMapa-1)||(y==0||y==AnchoMapa-2)){
                    Texture render;
                    render = columna;
                    batch.draw(render,
                            x * Settings.SCALED_TILE_SIZE,
                            y * Settings.SCALED_TILE_SIZE,
                            Settings.SCALED_TILE_SIZE,
                            Settings.SCALED_TILE_SIZE * 2f);
                }
            }
        }


            batch.draw(puerta,Settings.SCALED_TILE_SIZE * 11, Settings.SCALED_TILE_SIZE * 22,40,40);

            if (Settings.SCALED_TILE_SIZE * 22 == player.getY()*Settings.SCALED_TILE_SIZE && Settings.SCALED_TILE_SIZE * 11 == player.getX()*Settings.SCALED_TILE_SIZE){
                MainClass.trans = false;
            }

            //Si pisas las coordenadas donde esta la trampa mueres
            if (Settings.SCALED_TILE_SIZE * posYTrampa1 == player.getY()*Settings.SCALED_TILE_SIZE && Settings.SCALED_TILE_SIZE * posXTrampa1 == player.getX()*Settings.SCALED_TILE_SIZE){
                MainClass.muerto = false;
            }

            //Enemigos
            if (Settings.SCALED_TILE_SIZE * 15 == player.getY()*Settings.SCALED_TILE_SIZE && Settings.SCALED_TILE_SIZE * 8 == player.getX()*Settings.SCALED_TILE_SIZE){
                MainClass.muerto = false;
            }


            if ( player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 3 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3
                    || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 2
                    || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 8 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random2
                    || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random1 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 5){
                MainClass.muerto = false;
            }




            int coorX = 10, coorY = 10;

            if ( player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random1 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random2
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random4 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random1 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random2
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 15 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 8){
                MainClass.muerto = false;
            }



            for (int cnt=0; cnt<9; cnt++){
                    batch.draw(laba, Settings.SCALED_TILE_SIZE * (1 + cnt), Settings.SCALED_TILE_SIZE * (21), 40, 40);
                    if (player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (21) && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (1 + cnt)){
                        varenemigo = true;
                    }
            }

            for (int cnt=0; cnt<10; cnt++){
                batch.draw(laba, Settings.SCALED_TILE_SIZE * (13 + cnt), Settings.SCALED_TILE_SIZE * (21), 40, 40);
                if (player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (21) && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (13 + cnt)){
                    varenemigo = true;
                }
            }

            for (int cnt=0; cnt<22; cnt++){
                batch.draw(laba, Settings.SCALED_TILE_SIZE * (1 + cnt), Settings.SCALED_TILE_SIZE * (2), 40, 40);
                if (player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (2) && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * (1 + cnt)){
                    varenemigo = true;
                }
            }

            if(varenemigo == true){
                batch.draw(enemigo2,Settings.SCALED_TILE_SIZE * coorX + Settings.SCALED_TILE_SIZE * 1, Settings.SCALED_TILE_SIZE * coorY ,70,80);
                    MainClass.muerto = false;
            }





                batch.draw(nuevoPersonaje,
                        player.getX() * Settings.SCALED_TILE_SIZE,
                        player.getY() * Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE * 1.5f);  //1.5f Es para que el personaje no se muestre exactamente dentro del cuadrado
                System.out.println(player.getX() * Settings.SCALED_TILE_SIZE);



        batch.end();


    }

}
