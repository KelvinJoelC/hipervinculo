package com.hiperviculo.Pantallas;

import com.badlogic.gdx.Screen;
import com.hiperviculo.Controles.MainClass;

import javax.swing.*;
import java.util.Collection;

public abstract class AbstractScreen implements Screen {
    protected MainClass main;

    public AbstractScreen(MainClass main) {
        this.main = main;
    }



    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

