package com.hiperviculo.Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hiperviculo.Controles.MainClass;
import com.hiperviculo.Controles.PlayerController;
import com.hiperviculo.Controles.Settings;
import com.hiperviculo.Objetos.CasillasMapa;
import com.hiperviculo.Objetos.Player;

public class FinalScreen extends AbstractScreen {

    private PlayerController control;


    private Player player;
    private CasillasMapa mapa;
    private Texture nuevoPersonaje;
    private  Texture columna;
    private Texture Suelo;
    public SpriteBatch batch;
    private Texture trampa;
    private Texture enemigo1;
    private boolean puertallave = false;
    private Texture enemigo2;
    private Texture enemigo3;

    private int random1 = (int) (Math.random()*20);
    private int random2 = (int) (Math.random()*20);

    private int random3 = (int) (Math.random()*20);
    private int random4 = (int) (Math.random()*20);


    int AnchoMapa = 24, AltoMapa= 24 ;

    private Texture puerta;
    private Texture llave;


    public FinalScreen(MainClass main) {
        super(main);

        nuevoPersonaje = new Texture("PersonajePrincipal.png");
        Suelo = new Texture("SueloCastillo.PNG");
        columna = new Texture("columna2.png");
        batch = new SpriteBatch();
        mapa = new CasillasMapa(AnchoMapa, AltoMapa);
        // Creo el mapa de 24x24
        player = new Player(mapa, (AnchoMapa/2)-1, 2);
        // (El tamaño del las casillas por donde se moverá, x inicio, y inicio)
        control = new PlayerController(player);

        puerta = new Texture("puerta.png");
        trampa = new Texture("trampaResolucion1000.png");
        enemigo1 = new Texture("enemigo1.png");
        llave = new Texture("llave.png");
        enemigo2 = new Texture("enemigo1.png");
        enemigo3 = new Texture("enemigo1.png");



    }


    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(control);

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Segun el boton de jugar seleccionado, cargara un personaje u otro
        if (MainClass.personaje == true ){
            nuevoPersonaje = new Texture("PersonajePrincipal.png");
        }
        if (MainClass.personaje == false){
            nuevoPersonaje = new Texture("Elbueno_Principal.png");
        }

        batch.begin();


        for(int x=0; x < mapa.getAncho(); x++){
            for(int y =0; y < mapa.getAlto(); y++){
                Texture render ;
                render = Suelo;
                batch.draw(render, x* Settings.SCALED_TILE_SIZE, y*Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE,
                        Settings.SCALED_TILE_SIZE);
            }//(Textura que queremos cargar, X, Y, Ancho de como se va a ver la textura, Alto ... )

        }


        //Introducimos el personaje, esto solo lo dibuja y representa
        for(int x=0; x < AnchoMapa; x++){
            for(int y =0; y < AltoMapa; y++) {
                if ((x== 0 || x==AltoMapa-1)||(y==0||y==AnchoMapa-2)){
                    Texture render;
                    render = columna;
                    batch.draw(render,
                            x * Settings.SCALED_TILE_SIZE,
                            y * Settings.SCALED_TILE_SIZE,
                            Settings.SCALED_TILE_SIZE,
                            Settings.SCALED_TILE_SIZE * 2f);
                }
            }
        }




        //Que al coger la llave gane
        if (Settings.SCALED_TILE_SIZE * random2 == player.getY()*Settings.SCALED_TILE_SIZE && Settings.SCALED_TILE_SIZE * random1 == player.getX()*Settings.SCALED_TILE_SIZE){
            puertallave = true;
        }



        if (Settings.SCALED_TILE_SIZE * 22 == player.getY()*Settings.SCALED_TILE_SIZE && Settings.SCALED_TILE_SIZE * 11 == player.getX()*Settings.SCALED_TILE_SIZE && puertallave == true){
            MainClass.ganado = false;
        }



        batch.draw(puerta,Settings.SCALED_TILE_SIZE * 11, Settings.SCALED_TILE_SIZE * 0,40,40);

        batch.draw(llave,Settings.SCALED_TILE_SIZE * random1 , Settings.SCALED_TILE_SIZE * random2,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE);

        if(puertallave == true){
            batch.draw(puerta,Settings.SCALED_TILE_SIZE * 11, Settings.SCALED_TILE_SIZE * 22,40,40);
            //llave.dispose();
            batch.draw(Suelo, Settings.SCALED_TILE_SIZE * random1 , Settings.SCALED_TILE_SIZE * random2,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE);
        }


        batch.draw(enemigo3,Settings.SCALED_TILE_SIZE * random3, Settings.SCALED_TILE_SIZE * random3,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo3,Settings.SCALED_TILE_SIZE * random2, Settings.SCALED_TILE_SIZE * random1,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo2,Settings.SCALED_TILE_SIZE * random3, Settings.SCALED_TILE_SIZE * random4,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);
        batch.draw(enemigo2,Settings.SCALED_TILE_SIZE * random1, Settings.SCALED_TILE_SIZE * random3,Settings.SCALED_TILE_SIZE ,Settings.SCALED_TILE_SIZE * 1.5f);


        if ( player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random1 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random2
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random4 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random3
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random1 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * random2
                || player.getY()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 15 && player.getX()*Settings.SCALED_TILE_SIZE == Settings.SCALED_TILE_SIZE * 8){
            MainClass.muerto = false;
        }


        batch.draw(nuevoPersonaje,
                player.getX() * Settings.SCALED_TILE_SIZE,
                player.getY() * Settings.SCALED_TILE_SIZE,
                Settings.SCALED_TILE_SIZE,
                Settings.SCALED_TILE_SIZE * 1.5f);  //1.5f Es para que el personaje no se muestre exactamente dentro del cuadrado
        System.out.println(player.getX() * Settings.SCALED_TILE_SIZE);


        batch.end();

    }

}

